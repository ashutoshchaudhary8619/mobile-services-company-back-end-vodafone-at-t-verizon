CREATE DATABASE samtt;
GRANT ALL ON samtt.* TO 'root'@'localhost';

use samtt;

-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 06, 2016 at 06:39 AM
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `samtt`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `CountRecentRequests` (OUT `total` INT)  BEGIN 
SELECT count(id) INTO total from mo where created_at > DATE_SUB(NOW(), INTERVAL 15 MINUTE);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CountUnprocessedRequests` (OUT `total` INT)  BEGIN 
SELECT count(id) INTO total from mo where auth_token IS NULL;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertrequests` (IN `msisdn` VARCHAR(20), IN `operatorid` INT(11), IN `shortcodeid` INT(10), IN `text` VARCHAR(255), IN `auth_token` VARCHAR(60))  BEGIN 
insert into samtt.mo(`msisdn`,`operatorid`,`shortcodeid`, `text`, `auth_token`) values(msisdn ,operatorid,shortcodeid, text, auth_token);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `LastTenkMinMaxDates` (OUT `minDate` VARCHAR(30), OUT `maxDate` VARCHAR(30))  BEGIN
SELECT MIN(created_at), MAX(created_at) INTO minDate, maxDate FROM (SELECT created_at FROM mo ORDER BY id DESC LIMIT 10000 OFFSET 0) tmp;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `RemoveUnprocessedMos` ()  BEGIN
   DELETE  FROM mo where `auth_token` IS NULL;
   END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mo`
--

CREATE TABLE `mo` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `operatorid` int(11) DEFAULT NULL,
  `shortcodeid` int(11) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `auth_token` varchar(60) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mo`
--
ALTER TABLE `mo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mo`
--
ALTER TABLE `mo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

