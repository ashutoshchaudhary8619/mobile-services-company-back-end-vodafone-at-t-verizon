echo 'Start copy nginx configurations'

rm -rf /etc/nginx/sites-enabled/*

if [ -f /vagrant/conf/nginx/nginx.conf ]; then
  rm -f /etc/nginx/nginx.conf
  cp /vagrant/conf/nginx/nginx.conf /etc/nginx/nginx.conf
fi

if [ -f /vagrant/conf/nginx/conf.d/default.conf ]; then
  rm -f /etc/nginx/conf.d/default.conf
  cp /vagrant/conf/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
fi


echo 'Start copy sysctl configuration'

if [ -f /vagrant/conf/sysctl/sysctl.conf ]; then
  rm -f /etc/sysctl.conf
  cp /vagrant/conf/sysctl/sysctl.conf /etc/sysctl.conf
fi


echo 'Start copy PHP FPM configuration'

if [ -f /vagrant/conf/php/www.conf ]; then
  rm -f /etc/php/7.0/fpm/pool.d/www.conf
  cp /vagrant/conf/php/www.conf /etc/php/7.0/fpm/pool.d/www.conf
fi


echo 'Start copy security Limits configuration'

if [ -f /vagrant/conf/security/limits.conf ]; then
  rm -f /etc/security/limits.conf
  cp /vagrant/conf/security/limits.conf /etc/security/limits.conf
fi


echo 'Start copy mysql configuration'

if [ -f /vagrant/conf/mysql/mysql.cnf ]; then
  rm -f /etc/mysql/mysql.cnf
  cp /vagrant/conf/mysql/mysql.cnf /etc/mysql/mysql.cnf
fi

if [ -d /var/www/ ]; then
  cp -a /vagrant/www/. /var/www/
fi

