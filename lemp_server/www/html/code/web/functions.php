<?php
class DB_Con

	{
	protected static $DB_HOST = 'localhost';
	protected static $DB_USER = 'root';
	protected static $DB_PASS = 'root';
	protected static $DB_NAME = 'samtt';
	
	public static function connection()
		{
		try
			{
			$DB = new PDO(sprintf("mysql:host=%s;dbname=%s", self::$DB_HOST, self::$DB_NAME) , self::$DB_USER, self::$DB_PASS, array(
				PDO::ATTR_PERSISTENT => true
			));

			// set the PDO error mode to exception

			$DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}

		catch(PDOException $e)
			{
			echo 'Connection failed: ' . $e->getMessage();
			}

		return $DB;
		}
	}

class Mo extends DB_Con

	{
	private $DB;
	function __construct()
		{
		$this->DB = parent::connection();
		}

	public function get_auth_token()
		{
		$arg = json_encode($_REQUEST);
		/** Currently Using the PHP based new Register Mo tool which produces the same code as the Old tool but thousands of times faster
		 The problem with Old provided tool was that it cant cope up and crashes where as php now runs at thousads of requests per second
		 While the old tool could only run at 5-6 requests per second and cant handle any more so the old register mo tool crashes.
		 As it cant cope up with new fast PHP Code. *
		 */
		$processed_code = $this->RegisterMo();
		/** Uncomment the Below line if you want to use OLD Linux Based Obsolete Register Mo Tool and comment the above one **/

		 //$processed_code = `./registermo $arg`;
		 
		 
		 //If you want to artificially generate unprocessed codes please uncomment the below line
		 
		 //$processed_code = "";

		return $processed_code;
		}

	public function save($token)
		{
		if (!empty($_GET['msisdn']) && !empty($_GET['operatorid']) && !empty($_GET['shortcodeid']) && !empty($_GET['text']))
			{
			if (empty($token))
				{
				$token = NULL;
				}

			if (is_numeric($_GET['operatorid']) && is_numeric($_GET['shortcodeid']))
				{
				$stmt = $this->DB->prepare("CALL insertrequests(?, ?, ?, ?, ?)");
				$stmt->bindParam(1, $_GET['msisdn'], PDO::PARAM_STR, 20);
				$stmt->bindParam(2, $_GET['operatorid'], PDO::PARAM_INT, 11);
				$stmt->bindParam(3, $_GET['shortcodeid'], PDO::PARAM_INT, 10);
				$stmt->bindParam(4, $_GET['text'], PDO::PARAM_STR, 255);
				$stmt->bindParam(5, $token, PDO::PARAM_STR, 60);

				// call the stored procedure

				$stmt->execute();
				}
			  else
				{
				die("Please check the url for correct data variable types");
				}
			}
		  else
			{
			die("Unable to Process, Please input all the required parameters");
			}
		}

	public function DeleteUnprocessedMos()
		{
		$stmt = $this->DB->prepare("CALL RemoveUnprocessedMos()");
		$stmt->execute();
		$count = $stmt->rowCount();
		echo '{"Deleted Unprocessed Mos":"' . $count . '"}' . "\n";
		}

	public function ShowStats()
		{
		$response = array();
		$stmt = $this->DB->prepare("CALL CountRecentRequests(@total)");
		$stmt->execute();
		$stmt->closeCursor();
		$output = $this->DB->query("select @total")->fetch(PDO::FETCH_ASSOC);
		$response['last_15_min_mo_count'] = $output['@total'];
		$stmt = $this->DB->prepare("CALL LastTenkMinMaxDates(@minDate, @maxDate)");
		$stmt->execute();
		$stmt->closeCursor();
		$output = $this->DB->query("select @minDate, @maxDate")->fetch(PDO::FETCH_ASSOC);
		$response['time_span_last_10k'] = array(
			$output['@minDate'],
			$output['@maxDate']
		);
		echo json_encode($response) . "\n";
		}

	public function ShowUnprocessedMos()
		{
		$response = array();
		$stmt = $this->DB->prepare("CALL  CountUnprocessedRequests(@total)");
		$stmt->execute();
		$stmt->closeCursor();
		$output = $this->DB->query("select @total")->fetch(PDO::FETCH_ASSOC);
		$response['Number Of Unprocessed Mos'] = $output['@total'];
		echo json_encode($response) . "\n";
		}

	public function RegisterMo($length = 24)
		{
		$vCode = "";
		$possible = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
		$i = 0;
		while ($i < $length)
			{
			$char = substr($possible, mt_rand(0, strlen($possible) - 1) , 1);
			if (!strstr($vCode, $char))
				{
				$vCode.= $char;
				$i++;
				}
			}

		return $vCode;
		}
	}

?>
